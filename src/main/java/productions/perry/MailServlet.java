package productions.perry;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MailServlet", urlPatterns = {"/sendmail"})
public class MailServlet extends HttpServlet {
    private static final Logger LOG
            = Logger.getLogger(MailServlet.class.getName());

    private static final String MAIL_SESSION_NAME = "java:comp/env/mail/Mail";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            String name = req.getParameter("name");
            String email = req.getParameter("email");
            String text = req.getParameter("message");
            Session sess = getSession();
            MimeMessage message = new MimeMessage(sess);
            message.setFrom(new InternetAddress(email, name));
            InternetAddress to[] = new InternetAddress[1];
            to[0] = new InternetAddress("info@perry.productions");
            message.setRecipients(Message.RecipientType.TO, to);
            message.setSubject("A message from " + name, "UTF-8");
            message.setText(text, "UTF-8");
            send(sess, message);
        } catch (NamingException | MessagingException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        resp.sendRedirect("https://" + req.getHeader("Host") + "/");
    }

    @Override
    public String getServletInfo() {
        return "Mail sender";
    }

    private static Session getSession() throws NamingException {
        Context context = new InitialContext();
        return (Session) context.lookup(MAIL_SESSION_NAME);
    }

    private static void send(Session sess, Message message)
            throws MessagingException {
        Transport trans = sess.getTransport("smtp");
        try {
            trans.connect();
            trans.sendMessage(message, 
                    message.getRecipients(Message.RecipientType.TO));
        } finally {
            trans.close();
        }
    }
}
