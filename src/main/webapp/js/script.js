$(function() {
    $("a[href^='#']").on("click", function(e) {
        e.preventDefault();
        var seltor = $(this).attr("href"),
            top = $(seltor).offset().top;
        $('html, body').animate({
            scrollTop: top
        }, 1000);
    });
    if (!MobileEsp.DetectTierIphone()) {
        // not a smartphone: remove the tel:links
        $("a[href^='tel:']").each(function() {
            var $this = $(this), $span = $("<span/>");
            $span.attr("class",$this.attr("class"));
            $span.text($this.text());
            $this.replaceWith($span);
        });
    }
});
